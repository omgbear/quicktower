import { useState } from "react"
import "./App.css"
import { GameController } from "./components/logic/GridController"
import { StartCard } from "./components/ux/StartCard"
import { useLevels } from "./lib/levels"

function App() {
  const [key, setKey] = useState(1)
  const [running, setRunning] = useState(false)
  const { currentLevel, advanceToNextLevel, resetToLevel, currentLevelIdx } = useLevels()

  const restartGame = (won: boolean | number) => {
    if (typeof won == "number") {
      resetToLevel(won)
    } else if (won) {
      if (!advanceToNextLevel()) {
        // game complete!
        alert("Congrats, you are a winner!")
        resetToLevel(0)
      }
    } else {
      resetToLevel(0)
    }
    setKey(key + 1)
  }

  const exitToMainMenu = () => {
    setRunning(false)
  }

  const content = running ? (
    <GameController
      key={key}
      gameComplete={restartGame}
      exitToMainMenu={exitToMainMenu}
      currentLevel={currentLevel}
      currentLevelIdx={currentLevelIdx}
    />
  ) : (
    <StartCard setRunning={setRunning} />
  )

  return (
    <div style={{ background: "url(QuickTower.jpg)" }} className="w-full aspect-video">
      {content}
    </div>
  )
}

export default App

import { Elemental } from "./types"

export const elementToColor: Record<Elemental, string> = {
  earth: "green",
  fire: "orange",
  water: "blue",
  wind: "white",
  metal: "grey",
}

// Sorts metal first, then elements by their index
// TODO: Support having 'home' element always sort first after metal
export function compareElemental(
  { element: a }: { element: Elemental },
  { element: b }: { element: Elemental }
) {
  if (a == b) return 0
  if (a == "metal") return -1
  if (b == "metal") return 1
  return elementalIndex[a] - elementalIndex[b]
}

const elementalIndex = {
  fire: 1,
  earth: 2,
  water: 3,
  wind: 4,
}

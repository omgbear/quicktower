import { useCallback, useState } from "react"
import { TPS_GOAL } from "./card"
import {
  addDistanceAtAngle,
  GRID_SIZE,
  groupByQuartile,
  groupByRC,
  pointInTriangle,
  RcToString,
  RCToXY,
  xyAngle,
  XYToRC,
} from "./grid"
import { ImmutableSet } from "./immutable"
import { AttackInstance, AttackTo, Elemental, RC, XY } from "./types"
import { notEmpty } from "./util"

export interface AttackHit {
  id: string
  damage: number
  element: Elemental
}

export interface Defender extends XY, RC {
  id: string
  // health: {
  //   current: number
  // }
}

export function newAttackInstance(attack: AttackTo): AttackInstance {
  return {
    id: `${Math.random()}`,
    origin: attack.origin,
    facingAngle: xyAngle(attack.origin, RCToXY(attack, GRID_SIZE / 2)),
    distanceFromOrigin: 0,
    firedAtTick: attack.firedAtTick,
    expiresAtTick: attack.firedAtTick + attack.attack.attackLifetimeTicks,
    element: attack.attack.element,
    damage: attack.attack.damage,
    pattern: attack.attack.pattern,
  }
}

export function useAttacks({ initialAttacks = [] }: { initialAttacks?: AttackInstance[] }) {
  const [attacks, setAttacks] = useState<ImmutableSet<AttackInstance>>(ImmutableSet(initialAttacks))

  const resolveAttacks = useCallback(
    function resolveAttacks<D extends Defender>(
      tick: number,
      defenders: D[]
    ): { hits: AttackHit[]; hitboxes: Hitboxes[] } {
      const hitboxes: Hitboxes[] = []

      const defByQuartile = groupByQuartile(defenders)
      const defByRC = groupByRC(defenders)
      // Break up grid into sub-tiles, currently 1 quarter size.
      const activeAttacks = attacks.filter(({ expiresAtTick }) => expiresAtTick > tick)

      const hits: AttackHit[] = []
      activeAttacks.forEach((attack) => {
        const { quartiles, rcs } = getHitboxes(attack)
        hitboxes.push({ quartiles, rcs })
        for (const rc of quartiles) {
          if (defByQuartile[rc]) {
            hits.push(
              ...(defByQuartile[rc] || []).map(({ id }) => ({
                id,
                damage: attack.damage,
                element: attack.element,
              }))
            )
          }
        }

        for (const rc of rcs) {
          if (defByRC[rc]) {
            hits.push(
              ...(defByRC[rc] || []).map(({ id }) => ({ id, damage: attack.damage, element: attack.element }))
            )
          }
        }
      })
      return { hits, hitboxes }
    },
    [attacks]
  )

  const startAttacks = useCallback(
    function startAttacks(tick: number, newAttacks: AttackTo[]) {
      const newAttackInstances = newAttacks.map(newAttackInstance)
      const bornDead = newAttackInstances.filter(({ expiresAtTick }) => expiresAtTick < tick)
      if (bornDead.length) console.log("bd", ...bornDead)

      setAttacks(attacks.concat(newAttackInstances).filter(({ expiresAtTick }) => expiresAtTick > tick))
    },
    [attacks, setAttacks]
  )

  return { attacks, resolveAttacks, startAttacks }
}
export interface Hitboxes {
  quartiles: string[]
  rcs: string[]
  //mps?: XY[]
}

const hitboxCache: Record<string, Hitboxes> = {}

export function getHitboxes(attack: AttackInstance): Hitboxes {
  //try caching
  const cacheKey = hitboxCacheKey(attack)
  const cachedValue = hitboxCache[cacheKey]
  if (cachedValue) return cachedValue
  const hitboxes = getHitboxes_noCache(attack)
  hitboxCache[cacheKey] = hitboxes
  return hitboxes
}

export function hitboxCacheKey(attack: AttackInstance): string {
  const cacheKeys = [
    attack.distanceFromOrigin,
    attack.facingAngle,
    attack.origin.x,
    attack.origin.y,
    ...Object.values(attack.pattern),
  ]
  return cacheKeys.join(" ")
}

export function getHitboxes_noCache(attack: AttackInstance): Hitboxes {
  switch (attack.pattern.pattern) {
    case "bullet":
      throw new Error("NYI")
    case "line":
      throw new Error("NYI")
    case "fan":
      return getHitboxForFanAttack(attack)
  }
}

function getHitboxForFanAttack(attack: AttackInstance): Hitboxes {
  if (attack.pattern.pattern != "fan") throw new Error("Fan attack required")
  // find far left, far right
  // fit rectangle around it
  // for each quartile, if midpoint is inside
  // if all 4 inside, add to rcs, else add to quartiles
  const { farStart, farEnd } = getXYExtantsOfFan(attack)
  const minX = Math.min(attack.origin.x, farEnd.x, farStart.x)
  const maxX = Math.max(attack.origin.x, farEnd.x, farStart.x)
  const minY = Math.min(attack.origin.y, farEnd.y, farStart.y)
  const mayY = Math.max(attack.origin.y, farEnd.y, farStart.y)
  const minRC = XYToRC({ x: minX, y: minY })
  const maxRC = XYToRC({ x: maxX, y: mayY })
  const quartiles: string[] = []
  const rcs: string[] = []
  //const mps: XY[] = []
  for (let row = minRC.row; row <= maxRC.row; row++) {
    for (let col = minRC.col; col <= maxRC.col; col++) {
      const qs = midpointOffsets
        .map(([offsetX, offsetY, [offsetRow, offsetCol]]) => {
          if (
            pointInTriangle(
              { x: col * GRID_SIZE + offsetX, y: row * GRID_SIZE + offsetY },
              attack.origin,
              farStart,
              farEnd
            )
          ) {
            //            mps.push({ x: col * GRID_SIZE + offsetX, y: row * GRID_SIZE + offsetY })
            return RcToString({ row: row + offsetRow, col: col + offsetCol })
          }
        })
        .filter(notEmpty)
      // eslint-disable-next-line no-constant-condition
      if (false && qs.length == 4) {
        rcs.push(RcToString({ row, col }))
      } else {
        quartiles.push(...qs)
      }
    }
  }

  return { quartiles, rcs }
}
const midpointOffsets: [number, number, [number, number]][] = [
  [GRID_SIZE / 4, GRID_SIZE / 4, [0, 0]],
  [(GRID_SIZE / 4) * 3, GRID_SIZE / 4, [0, 0.5]],
  [(GRID_SIZE / 4) * 3, (GRID_SIZE / 4) * 3, [0.5, 0.5]],
  [GRID_SIZE / 4, (GRID_SIZE / 4) * 3, [0.5, 0]],
]

export function getXYExtantsOfFan(attack: AttackInstance) {
  if (attack.pattern.pattern != "fan") throw new Error("Fan attack required")
  const farStart = addDistanceAtAngle(
    attack.origin,
    attack.facingAngle - attack.pattern.angle / 2,
    attack.pattern.distance
  )
  const farEnd = addDistanceAtAngle(
    attack.origin,
    attack.facingAngle + attack.pattern.angle / 2,
    attack.pattern.distance
  )

  const closeStart = addDistanceAtAngle(attack.origin, attack.facingAngle - attack.pattern.angle / 2, 20)
  const closeEnd = addDistanceAtAngle(attack.origin, attack.facingAngle + attack.pattern.angle / 2, 20)

  return { farStart, farEnd, closeStart, closeEnd }
}

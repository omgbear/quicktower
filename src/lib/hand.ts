import { Card } from "./card"

export type InHand<T> = T & {
  raised: "all" | "some" | "none"
}

export function RaiseCard(card: Card, raised: InHand<Card>["raised"] = "none"): InHand<Card> {
  return { ...card, raised }
}

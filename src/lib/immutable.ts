import { Set, Map, List } from "immutable"

export { Set as ImmutableSet, Map as ImmutableMap, List as ImmutableList }

export function trackedGet<T>(m: { get: (k: string) => T }) {
  const results: Record<string, number> = {}
  return [
    (k: string): T => {
      const r = m.get(k)
      if (r != undefined) {
        results[k] = (results[k] || 0) + 1
      }
      return r
    },
    () => results,
  ]
}

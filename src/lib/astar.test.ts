import { BestPath, PrintPath } from "./astar"

type Point = {
  col: number
  row: number
}

it("computes a path", () => {
  const score = (at: Point): number => (at.col > 10 || at.row > 10 || at.col < 0 || at.row < 0 ? Infinity : 1)

  const expectedPath = `X00000
XXXX00
000X00
000XX0
0000X0
0000XX
`

  expect(PrintPath(BestPath(1, { row: 0, col: 0 }, { row: 5, col: 5 }, 1, score))).toEqual(expectedPath)
})

it("passes obstacles", () => {
  const score = (at: Point): number =>
    at.col > 10 ||
    at.row > 10 ||
    at.col < 0 ||
    at.row < 0 ||
    (at.row === 1 && at.col !== 9) ||
    (at.row === 3 && at.col !== 1)
      ? Infinity
      : 1
  const expectedPath = `XXXXXXXXXX
000000000X
0XXXXXXXXX
0X00000000
0X00000000
0XXXXX0000
`

  expect(PrintPath(BestPath(1, { row: 0, col: 0 }, { row: 5, col: 5 }, 1, score))).toEqual(expectedPath)
})

it("returns [] when there is no path", () => {
  const score = (at: Point): number =>
    at.col > 10 || at.row > 10 || at.col < 0 || at.row < 0 || at.row === 1 ? Infinity : 1

  const path = BestPath(1, { row: 0, col: 0 }, { row: 5, col: 5 }, 1, score)
  expect(path).toEqual([])
})

xit("computes a scaled path", () => {
  const score = (at: Point): number =>
    at.col > 100 || at.row > 100 || at.col < 0 || at.row < 0 ? Infinity : 1

  const expectedPath = `XX0000
0X0000
0XXX00
000X00
000XX0
0000XX
`

  expect(PrintPath(BestPath(10, { row: 0, col: 0 }, { row: 50, col: 50 }, 1, score), 10)).toEqual(
    expectedPath
  )
})

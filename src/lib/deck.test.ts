import { List } from "immutable"
import { GetCard } from "./card"
import { discardCards, drawCards, newDeckInstance, PlayCardResult, playCards, playCardsFn } from "./deck"
import { ImmutableList } from "./immutable"

const deck = ImmutableList([
  GetCard("fireball"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("watertower"),
  GetCard("watertower"),
  GetCard("watertower"),
])

describe("Deck", () => {
  it("initializes properly", () => {
    const di = newDeckInstance(deck)
    expect(di.burnt.size).toBe(0)
    expect(di.discard.size).toBe(0)
    expect(di.hand.size).toBe(0)
    expect(di.proto.size).toBe(10)
    expect(di.draw.size).toBe(10)
  })

  it("Draws cards", () => {
    const di = drawCards(newDeckInstance(deck), 7).deck
    expect(di.discard.size).toBe(0)
    expect(di.hand.size).toBe(7)
    expect(di.draw.size).toBe(3)
    expect(di.proto.size).toBe(10)
  })

  it("Discards cards", () => {
    const di = discardCards(drawCards(newDeckInstance(deck), 7).deck, 5).deck

    expect(di.discard.size).toBe(5)
    expect(di.hand.size).toBe(2)
    expect(di.draw.size).toBe(3)
    expect(di.proto.size).toBe(10)
  })

  it("Shuffles cards", () => {
    const di = drawCards(discardCards(drawCards(newDeckInstance(deck), 7).deck, 7).deck, 7).deck

    expect(di.discard.size).toBe(0)
    expect(di.hand.size).toBe(7)
    expect(di.draw.size).toBe(3)
    expect(di.proto.size).toBe(10)
  })

  it("Plays cards, discarding them", async () => {
    let di = drawCards(newDeckInstance(deck), 7).deck
    const expectedNames = di.hand.map(({ name }) => name).slice(2, 6)
    di = (await playCards(di, playCardsFn(2, 4, "discard"))).deck
    expect(di.burnt.size).toBe(0)
    expect(di.hand.size).toBe(3)
    expect(di.draw.size).toBe(3)
    expect(di.discard.map(({ name }) => name)).toEqual(expectedNames)
  })

  it("Plays cards, burning them, wrapping", async () => {
    let di = drawCards(newDeckInstance(deck), 7).deck
    const expectedNames = di.hand
      .map(({ name }) => name)
      .slice(0, 2)
      .concat(di.hand.map(({ name }) => name).slice(4, 7))
    di = (await playCards(di, playCardsFn(4, 5, "burnt"))).deck

    expect(di.discard.size).toBe(0)
    expect(di.hand.size).toBe(2)
    expect(di.draw.size).toBe(3)
    expect(di.burnt.map(({ name }) => name)).toEqual(expectedNames)
  })
})

import { RC, XY } from "./types"

export const GRID_SIZE = 50
export const GRID_COUNT = 14

const mid = Math.floor(GRID_COUNT / 2)
export const midCoords = { row: mid, col: mid }

export function XYToRC({ x, y }: { x: number; y: number }): RC {
  return {
    row: Math.floor(y / GRID_SIZE),
    col: Math.floor(x / GRID_SIZE),
  }
}

export function RCToXY({ row, col }: RC, offset: number | XY = 0): XY {
  const [offsetX, offsetY] = typeof offset == "number" ? [offset, offset] : [offset.x, offset.y]
  return { y: row * GRID_SIZE + offsetX, x: col * GRID_SIZE + offsetY }
}

export function coordsEqual(a: RC | undefined, b: RC | undefined): boolean {
  return a?.col == b?.col && a?.row == b?.row
}

export function RcToString({ row, col }: RC): string {
  return `${row}:${col}`
}

export function StringToRc(s: string): RC {
  const parts = s.split(":")
  if (parts.length != 2) {
    throw new Error("Not an RC String: " + s)
  }
  return {
    row: parseFloat(parts[0]),
    col: parseFloat(parts[1]),
  }
}

export function distance(start: RC, goal: RC): number {
  return Math.abs(goal.row - start.row) + Math.abs(goal.col - start.col)
}

export function groupByRC<T extends RC>(items: T[]): Record<string, T[]> {
  const mobsByRC: Record<string, T[]> = {}
  items.forEach((item) => {
    const rc = RcToString(item)
    mobsByRC[rc] ||= []
    mobsByRC[rc].push(item)
  })
  return mobsByRC
}

export function groupByQuartile<T extends XY>(items: T[]): Record<string, T[]> {
  const mobsByRC: Record<string, T[]> = {}
  items.forEach((item) => {
    const doubleRC = XYToRC({ x: item.x * 2, y: item.y * 2 })
    const rc = RcToString({ row: doubleRC.row / 2, col: doubleRC.col / 2 })
    mobsByRC[rc] ||= []
    mobsByRC[rc].push(item)
  })
  return mobsByRC
}

export function rcAngle(origin: RC, target: RC) {
  const originXY = RCToXY(origin, GRID_SIZE / 2),
    targetXY = RCToXY(target, GRID_SIZE / 2)
  return xyAngle(originXY, targetXY)
}

export function xyAngle(origin: XY, target: XY) {
  return Math.atan2(origin.y - target.y, origin.x - target.x) - Math.PI
}

export function xyDistance(start: XY, goal: XY) {
  return Math.sqrt(Math.pow(goal.x - start.x, 2) + Math.pow(goal.y - start.y, 2))
}

export function addDistanceAtAngle(start: XY, angle: number, distance: number): XY {
  const x = start.x + Math.cos(angle) * distance
  const y = start.y + Math.sin(angle) * distance
  return { x, y }
}

function sign(p1: XY, p2: XY, p3: XY) {
  return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y)
}

export function pointInTriangle(pt: XY, v1: XY, v2: XY, v3: XY) {
  const d1 = sign(pt, v1, v2),
    d2 = sign(pt, v2, v3),
    d3 = sign(pt, v3, v1),
    has_neg = d1 < 0 || d2 < 0 || d3 < 0,
    has_pos = d1 > 0 || d2 > 0 || d3 > 0

  return !(has_neg && has_pos)
}

export interface Tile extends RC {
  kind: string
  subkind: string
}

export interface Tower extends Tile {
  kind: "tower"
  subkind: string
  element: Elemental
  id: string
  health: {
    current: number
    max: number
  }
}

export function isTower(t: Tile): t is Tower {
  return t.kind == "tower"
}

export interface TargetedTower extends Tower {
  target?: RC
  angle: number
}

export interface AttackingTower extends Tower {
  attacks: AttackFrom[]
}

export interface AttackingTargetedTower extends TargetedTower, AttackingTower {}

export function isTargetedTower(t: Tower): t is TargetedTower {
  return typeof (t as TargetedTower).angle == "number"
}

export function isAttackingTower(t: Tower): t is AttackingTower {
  return typeof (t as AttackingTower).attacks != undefined
}

export interface RC {
  row: number
  col: number
}
export interface XY {
  x: number
  y: number
}

export function isMob(t: Tile): t is Mob {
  return t.kind == "mob"
}

export interface MobStats {
  speed: number
  health: {
    current: number
    max: number
  }
}

export interface Mob extends XY, Tile, MobStats {
  kind: "mob"
  offsetX: number
  offsetY: number
  jitter: number
  id: string
  distanceFromEnd: number
}

export type Path = WithDirection<RC>[]

export type Direction = "U" | "D" | "L" | "R" | "S"

export type WithDirection<T> = T & { direction: Direction }

export type AttackTo = RC & {
  attack: Attack
  firedAtTick: number
  origin: XY
}

export type AttackFrom = Attack & {
  lastFiredAt: number
}

export function NewAttackTo(firedAtTick: number, origin: XY, t: RC, attack: Attack): AttackTo {
  return {
    row: t.row,
    col: t.col,
    attack,
    origin,
    firedAtTick,
  }
}

export type TargetKind = "all" | "enemy" | "tower" | "ground" | "melee"
export type Elemental = "water" | "fire" | "earth" | "wind" | "metal"

export type Attack = {
  targetKind: TargetKind
  element: Elemental
  damage: number
  interAttackTicks: number
  pattern: AttackPattern
  attackLifetimeTicks: number
}

type AttackPattern =
  | {
      pattern: "bullet"
      maxDistance: number
    }
  | {
      pattern: "line"
      distance: number
      width: number
    }
  | {
      pattern: "fan"
      distance: number
      angle: number
    }

export type Tick = number

export type AttackInstance = {
  id: string
  origin: XY
  facingAngle: number
  distanceFromOrigin: number
  firedAtTick: Tick
  expiresAtTick: Tick
  element: Elemental
  damage: number
  pattern: AttackPattern
}

export interface PathInfo {
  hash: string
  distanceFromEnd: number
  direction: Direction
}

export type Spawner = RC & {
  interval: number
  count: number
  mob: MobStats | (() => MobStats)
}

export interface Level {
  spawners: Spawner[]
  gameLengthMs: number
}

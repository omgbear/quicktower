import { ImmutableMap } from "./immutable"
import { Attack, Elemental } from "./types"

export interface Card {
  kind: "card"
  name: string
  subkind: "tower" | "spell"
  rarity: "common" | "uncommon" | "rare"
  element: Elemental
}

interface Aura {
  kind: "aura"
}

export interface TowerCard extends Card {
  subkind: "tower"
  startLevel: number
  auras: Aura[]
  attacks: Attack[]
  tiles: number
}

// export interface SpellCard extends Card {
//   subkind: "spell"
//   target: TargetKind
//   auras: Aura[]
//   attacks: Attack[]
// }

function newTowerCard(
  name: string,
  element: TowerCard["element"],
  rarity: TowerCard["rarity"],
  attacks: Attack[]
): TowerCard {
  return {
    kind: "card",
    subkind: "tower",
    name,
    element,
    rarity,
    auras: [],
    startLevel: 1,
    tiles: 1,
    attacks,
  }
}

export const TPS_GOAL = 30

export const cards = ImmutableMap<string, Card>({
  firetower: newTowerCard("Fire Tower", "fire", "common", [
    {
      element: "fire",
      targetKind: "melee",
      damage: 10 / TPS_GOAL,
      interAttackTicks: TPS_GOAL * 1,
      attackLifetimeTicks: TPS_GOAL * 0.8,
      pattern: {
        pattern: "fan",
        distance: 150,
        angle: Math.PI / 4,
      },
    },
  ]),
  watertower: newTowerCard("Water Tower", "water", "common", [
    {
      interAttackTicks: TPS_GOAL * 7,
      attackLifetimeTicks: TPS_GOAL * 1,
      element: "water",
      targetKind: "enemy",
      damage: 20 / TPS_GOAL,
      pattern: {
        pattern: "fan",
        distance: 250,
        //width: 4,
        angle: Math.PI / 10,
      },
    },
  ]),
})

export function GetCard(name: string): Card {
  const card = cards.get(name)
  if (!card) throw new Error(`Card ${name} not found.`)
  return card
}

export function MaybeGetCard(name: string): Card | undefined {
  return cards.get(name)
}

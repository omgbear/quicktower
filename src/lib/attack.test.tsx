import { act, renderHook } from "@testing-library/react"
import {
  AttackHit,
  Defender,
  getHitboxes_noCache,
  hitboxCacheKey,
  Hitboxes,
  newAttackInstance,
  useAttacks,
} from "./attack"
import { GRID_COUNT, GRID_SIZE, RCToXY } from "./grid"
import { Attack, AttackTo } from "./types"

function attackto({ pattern }: { pattern: Attack["pattern"]; facingAngle?: number }): AttackTo {
  return {
    row: 9,
    col: 10,
    origin: { x: 525, y: 525 },
    firedAtTick: 0,
    attack: {
      element: "fire",
      damage: 10,
      attackLifetimeTicks: 100,
      interAttackTicks: 1,
      targetKind: "ground",
      pattern,
    },
  }
}

const defenders: Defender[] = new Array(GRID_COUNT * GRID_COUNT).fill(0).map((n, idx) => {
  const row = idx % GRID_COUNT
  const col = Math.floor(idx / GRID_COUNT)
  const rc = { row, col }
  return { id: `${idx}`, ...rc, ...RCToXY(rc, GRID_SIZE / 2) }
})

describe("hitboxes", () => {
  const tests: {
    expected: Hitboxes
    attack: AttackTo
    expectedHits: AttackHit[]
  }[] = [
    {
      attack: attackto({ pattern: { pattern: "fan", angle: Math.PI / 6, distance: 100 } }),
      expected: { rcs: [], quartiles: ["8.5:10.5", "8.5:10", "9:10", "9:10.5"] },
      expectedHits: [{ damage: 10, element: "fire", id: "158" }],
    },
    {
      attack: attackto({
        pattern: { pattern: "fan", angle: Math.PI / 3, distance: 75 },
      }),
      expected: { rcs: ["9:10"], quartiles: [] },
      expectedHits: [{ damage: 10, element: "fire", id: "159" }],
    },
    {
      attack: attackto({
        pattern: { pattern: "fan", angle: Math.PI / 2, distance: 75 },
      }),
      expected: { rcs: [], quartiles: ["9.5:9.5", "9.5:10.5", "9.5:10", "9.5:11", "10:10", "10:10.5"] },
      expectedHits: [
        { damage: 10, element: "fire", id: "144" },
        { damage: 10, element: "fire", id: "159" },
      ],
    },
  ]

  tests.forEach((test) => {
    const attackInstance = newAttackInstance(test.attack)

    it("Attacks" + hitboxCacheKey(attackInstance), () => {
      const attackHookResult = renderHook(useAttacks, { initialProps: {} })
      expect(getHitboxes_noCache(attackInstance)).toEqual(test.expected)
      act(() => attackHookResult.result.current.startAttacks(0, [test.attack]))

      const { hits } = attackHookResult.result.current.resolveAttacks(0, defenders)
      expect(hits).toEqual(test.expectedHits)
    })
  })
})

import { groupByQuartile, XYToRC } from "./grid"
import { ImmutableSet } from "./immutable"
import { RC, XY } from "./types"

interface XYRC extends XY, RC {}

describe("grouping", () => {
  const mobs = ImmutableSet<XYRC>([
    mkmob(24, 24),
    mkmob(24, 25),
    mkmob(25, 24),
    mkmob(25, 25),
    mkmob(51, 51),
    mkmob(300, 300),
  ])

  it("by quartile", () => {
    const mobsByQ = groupByQuartile([...mobs])
    expect(mobsByQ).toEqual({
      "0.5:0": [mkmob(24, 25)],
      "0.5:0.5": [mkmob(25, 25)],
      "0:0.5": [mkmob(25, 24)],
      "0:0": [mkmob(24, 24)],
      "1:1": [mkmob(51, 51)],
      "6:6": [mkmob(300, 300)],
    })
  })
})

function mkmob(x: number, y: number): XYRC {
  const xy = { x, y }
  return { ...xy, ...XYToRC(xy) }
}

import { List } from "immutable"
import { useCallback, useState } from "react"
import { Card } from "./card"
import { compareElemental } from "./elemental"
import { ImmutableList, ImmutableSet } from "./immutable"
import { shuffle } from "./util"

export type Deck = ImmutableList<Card>

export function playCardsFn(
  startIdx: number,
  numPlayed: number,
  nextCardLocation: PlayCardResult["nextCardLocation"]
): <T>(cards: List<T>) => Promise<PlayCardResult[]> {
  return async <T>(cards: List<T>): Promise<PlayCardResult[]> => {
    if (numPlayed > cards.size) throw new Error("Not enough cards")
    const wrapping = startIdx + numPlayed > cards.size
    const start1 = wrapping ? 0 : startIdx
    const end1 = wrapping ? cards.size - numPlayed : startIdx + numPlayed
    const start2 = wrapping ? startIdx : Infinity
    return [
      ...cards.map((t, idx) => {
        if (idx >= start1 && idx < end1) {
          return { nextCardLocation } as PlayCardResult
        }
        if (idx >= start2) {
          return { nextCardLocation } as PlayCardResult
        }
        return { nextCardLocation: "hand" } as PlayCardResult
      }),
    ]
  }
}

export type DeckInstance = {
  proto: Deck
  draw: Deck
  hand: Deck
  discard: Deck
  burnt: Deck
}

export function useDeck(initialDeck: ImmutableList<Card> | ImmutableSet<Card> | Card[], initialHandSize = 7) {
  const [deck, setDeck] = useState(
    drawCards(newDeckInstance(ImmutableList(initialDeck)), initialHandSize).deck
  )

  const draw = useCallback(
    (numCards: number) => {
      const { deck: newDeck, drawn } = drawCards(deck, numCards)
      setDeck(newDeck)
      return drawn
    },
    [deck]
  )

  const discard = useCallback(
    (numCards: number) => {
      const { deck: newDeck, discarded } = discardCards(deck, numCards)
      setDeck(newDeck)
      return discarded
    },
    [deck]
  )

  const play = useCallback(
    async (playCardsFn: (cards: List<Card>) => Promise<PlayCardResult[]>, numDraw: number) => {
      const { deck: newDeck, discarded, burnt } = await playCards(deck, playCardsFn)
      if (!drawCards) {
        setDeck(newDeck)
        return { discarded, burnt }
      }
      const { deck: drawnDeck, drawn } = drawCards(newDeck, numDraw)
      setDeck(drawnDeck)
      return { discarded, burnt, drawn }
    },
    [deck]
  )

  return {
    deck,
    draw,
    play,
    discard,
  }
}

export function newDeckInstance(deck: Deck): DeckInstance {
  const draw = ImmutableList(shuffle([...deck]))

  return {
    proto: ImmutableList(deck),
    draw,
    hand: ImmutableList(),
    discard: ImmutableList(),
    burnt: ImmutableList(),
  }
}

export function discardCards(di: DeckInstance, numCards: number): { deck: DeckInstance; discarded: Card[] } {
  const discardCards: Card[] = []
  if (numCards > di.hand.size) throw new Error("Not enough cards in hand to discard")
  const hand = di.hand.skipWhile((card: Card, idx: number) => {
    if (idx < numCards) {
      discardCards.push(card)
      return true
    }
    return false
  })
  return {
    deck: {
      ...di,
      hand,
      discard: di.discard.concat(discardCards),
    },
    discarded: discardCards,
  }
}

export function drawCards(
  di: DeckInstance,
  numCards: number,
  maxHandSize = 10
): { deck: DeckInstance; drawn: Card[] } {
  if (di.hand.size + numCards > maxHandSize) throw new Error("Too many cards in hand")
  let draw = di.draw
  let discard = di.discard
  if (di.draw.size < numCards) {
    draw = di.draw.concat(shuffle([...di.discard]))
    discard = ImmutableList()
  }
  const drawnCards: Card[] = []
  draw = draw.skipWhile((card: Card, idx: number) => {
    if (idx < numCards) {
      drawnCards.push(card)
      return true
    }
    return false
  })

  return {
    drawn: drawnCards,
    deck: {
      ...di,
      draw,
      discard,
      hand: di.hand.concat(drawnCards).sort(compareElemental),
    },
  }
}

export async function playCards(
  di: DeckInstance,
  playCards: (cards: List<Card>) => Promise<PlayCardResult[]>
): Promise<{ deck: DeckInstance; discarded: Card[]; burnt: Card[] }> {
  const playedCardResult = [...(await playCards(di.hand))]
  if (playedCardResult.length != di.hand.size) throw new Error("Wrong amount of cards returned after play")
  const discardedCards: Card[] = []
  const burntCards: Card[] = []
  const handCards: Card[] = []
  di.hand.forEach((card, idx) => {
    switch (playedCardResult[idx].nextCardLocation) {
      case "discard":
        discardedCards.push(card)
        break
      case "burnt":
        burntCards.push(card)
        break
      case "hand":
        handCards.push(card)
        break
    }
  })

  return {
    discarded: discardedCards,
    burnt: burntCards,
    deck: {
      ...di,
      hand: ImmutableList(handCards),
      discard: di.discard.concat(discardedCards),
      burnt: di.burnt.concat(burntCards),
    },
  }
}

export interface PlayCardResult {
  nextCardLocation: "hand" | "discard" | "burnt"
}

import { useState } from "react"
import { GRID_COUNT } from "./grid"
import { Level } from "./types"

const TLW = 0, // Top/Left Wall
  BRW = GRID_COUNT - 1 // Bottom/Right Wall

const SPAWN_FAST = 50,
  SPAWN_MED = 300

const SPAWN_FEW = 3,
  SPAWN_SOME = 10,
  SPAWN_MANY = 20

const baseMob = () => ({
  speed: 1 + (Math.random() - 0.5),
  health: {
    current: 50,
    max: 50,
  },
})

const levels: Level[] = [
  {
    spawners: [{ row: TLW, col: 2, interval: SPAWN_FAST, count: SPAWN_SOME, mob: baseMob }],
    gameLengthMs: 50_000,
  },
  {
    spawners: [
      { row: TLW, col: 2, interval: SPAWN_FAST, count: SPAWN_SOME, mob: baseMob },
      { row: TLW, col: 7, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
    ],
    gameLengthMs: 60_000,
  },
  {
    spawners: [
      { col: BRW, row: 3, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
      { col: BRW, row: 8, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
    ],
    gameLengthMs: 60_000,
  },
  {
    spawners: [
      { row: TLW, col: 2, interval: SPAWN_FAST, count: SPAWN_SOME, mob: baseMob },
      { col: BRW, row: 8, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
    ],
    gameLengthMs: 60_000,
  },
  {
    spawners: [
      { row: TLW, col: 2, interval: SPAWN_FAST, count: SPAWN_SOME, mob: baseMob },
      { row: TLW, col: 8, interval: SPAWN_FAST, count: SPAWN_MANY, mob: baseMob },
      { col: BRW, row: 3, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
      { col: BRW, row: 8, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
    ],
    gameLengthMs: 60_000,
  },
  {
    spawners: [
      { row: TLW, col: 2, interval: SPAWN_FAST, count: SPAWN_SOME, mob: baseMob },
      { row: TLW, col: 8, interval: SPAWN_FAST, count: SPAWN_MANY, mob: baseMob },
      { col: BRW - 2, row: BRW - 2, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
      { col: BRW, row: BRW, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
    ],
    gameLengthMs: 60_000,
  },
  {
    spawners: [
      { row: TLW + 2, col: 2, interval: SPAWN_FAST, count: SPAWN_MANY, mob: baseMob },
      { row: TLW + 2, col: 8, interval: SPAWN_FAST, count: SPAWN_MANY, mob: baseMob },
      { col: BRW - 2, row: BRW - 2, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
      { row: TLW, col: 2, interval: SPAWN_FAST, count: SPAWN_SOME, mob: baseMob },
      { col: BRW, row: 8, interval: SPAWN_MED, count: SPAWN_MANY, mob: baseMob },
    ],
    gameLengthMs: 60_000,
  },
]

// TODO: Random level gen? Maybe generator to create static set
// Constraints like spawner count, min distance to HQ, etc.
// maybe assign some 'cost' to each spawner and each map has increasing cost
// greedily? generate spawners, consuming some cost
// target sides for spawners, etc.

export function useLevels(initialLevelIdx = 0) {
  const [currentLevelIdx, setCurrentLevelIdx] = useState(initialLevelIdx)

  function advanceToLevel(nextLevel?: number): boolean {
    if (nextLevel == undefined) nextLevel = currentLevelIdx + 1
    if (nextLevel >= levels.length) return false
    if (nextLevel < 0) return false
    setCurrentLevelIdx(nextLevel)
    return true
  }

  return {
    currentLevel: levels[currentLevelIdx],
    currentLevelIdx,
    advanceToNextLevel: advanceToLevel,
    resetToLevel: advanceToLevel,
  }
}

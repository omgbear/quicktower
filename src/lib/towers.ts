import { useCallback, useState } from "react"
import { TowerCard } from "./card"
import { GRID_SIZE, midCoords, RcToString, RCToXY, xyAngle } from "./grid"
import { ImmutableMap, ImmutableSet } from "./immutable"
import {
  AttackFrom,
  AttackingTargetedTower,
  AttackingTower,
  AttackTo,
  Elemental,
  isAttackingTower,
  isTargetedTower,
  Mob,
  RC,
  TargetedTower,
  Tower,
} from "./types"
import { getCurrentTick, notEmpty } from "./util"

function initialTower(): ImmutableMap<string, Tower> {
  const centerTile = newTower(midCoords, "metal", "HQ", 10_000)
  return ImmutableMap({ [RcToString(midCoords)]: centerTile })
}

export function newTower(rc: RC, element: Elemental, subkind: string, health = 1000): Tower | TargetedTower {
  return {
    id: `${Math.random()}`,
    ...rc,
    kind: "tower",
    element,
    subkind,
    health: {
      current: health,
      max: health,
    },
    target: undefined,
    angle: 0,
  }
}

export function newTowerFromCard(
  rc: RC,
  card: TowerCard,
  health = 1000
): Tower | TargetedTower | AttackingTargetedTower {
  console.log("ATK", card.attacks)
  return {
    id: `${Math.random()}`,
    ...rc,
    kind: "tower",
    element: card.element,
    subkind: card.name,
    health: {
      current: health,
      max: health,
    },
    attacks: card.attacks.map((attack) => ({ ...attack, lastFiredAt: 0 })),
    target: undefined,
    angle: 0,
  }
}

export function useTowers() {
  const [placedTowers, setPlacedTowers] = useState<ImmutableMap<string, Tower>>(initialTower())

  const getPlacedTower = useCallback(
    (coords: RC) => {
      const rcStr = RcToString(coords)
      return placedTowers.get(rcStr)
    },
    [placedTowers]
  )

  const attackTowers = (attacks: AttackTo[]) => {
    if (!attacks.length) return
    const attacksByRcs = attacks.reduce((accum: Record<string, number>, attack: AttackTo) => {
      const rcs = RcToString(attack)
      accum[rcs] = accum[rcs] || 0 + attack.attack.damage
      return accum
    }, {})
    setPlacedTowers(
      placedTowers
        .withMutations((towers) => {
          Object.entries(attacksByRcs).forEach(([rcs, damage]) => {
            towers.update(rcs, (tower) => {
              if (!tower) throw new Error("Missing tower")
              return {
                ...tower,
                health: { ...tower.health, current: Math.max(0, tower.health.current - damage * 0.5) },
              }
            })
          })
        })
        .filter((tower) => tower.health.current > 0)
    )
  }

  const placeTowers = useCallback(
    (towers: ImmutableMap<string, Tower>) => {
      setPlacedTowers(placedTowers.merge(towers))
    },
    [setPlacedTowers, placedTowers]
  )

  //const towerStrategies = ["maximizeTowerDamage", "closestToHQ", "highestMobDamage", "highestMobHealth"]
  // to visualize, click a tower to select it
  // draw squares to indicate value to tower -- green/yellow/red/etc
  const updateTowers = useCallback(
    function updateTowers(
      mobs: ImmutableSet<Mob>,
      startAttacks: (tick: number, attack: AttackTo[]) => void,
      hoverCol: (RC & { offsetX: number; offsetY: number }) | undefined
    ) {
      const tick = getCurrentTick()
      //const mobsByRC = groupByRC([...mobs])
      const collectedAttacks: AttackTo[] = []
      setPlacedTowers(
        placedTowers.map((tower) => {
          // updateTower
          //
          if (isAttackingTower(tower)) {
            if (isTargetedTower(tower)) {
              // check for new target
              // TODO: Re-target interval
              // enumerate all tiles in range
              // for each, calculate value
              // target that tower
              // start turning toward it
              // only fire when pointing at target?
              // Maybe fire then consider moving
              //const allmobs = Object.values(mobsByRC)
              const target = hoverCol //allmobs.length ? allmobs[0][0] : undefined
              //if(shouldStartAttack(tower)){
              //sstartAttack(tower)
              if (target) {
                const origin = RCToXY(tower, GRID_SIZE / 2)
                tower.target = target
                tower.angle = xyAngle(origin, RCToXY(tower.target, { x: target.offsetX, y: target.offsetY }))

                const attacks = doTowerAttacks(tick, tower)
                //console.log(attacks)
                if (attacks) {
                  //  console.log(tower.target)
                  tower.attacks = attacks
                  if (tower.target) {
                    collectedAttacks.push(
                      ...attacks
                        .filter(({ lastFiredAt }) => lastFiredAt == tick)
                        .map((attack) =>
                          tower.target
                            ? {
                                attack,
                                row: tower.target.row,
                                col: tower.target.col,
                                origin,
                                firedAtTick: tick,
                              }
                            : undefined
                        )
                        .filter(notEmpty)
                    )
                  }
                }
              }
            }
          }

          return tower
        })
      )
      //if (collectedAttacks.length)
      //console.log(collectedAttacks)
      startAttacks(tick, collectedAttacks)

      // update target
      // update angle
      // how can I maximize my damage over the next X ticks
      // should account for fire rate, turn speed, expected rotation, perhaps expected mob movement for far moves?
      // governed by player set strategy
    },
    [placedTowers]
  )

  return { getPlacedTower, placeTowers, attackTowers, placedTowers, updateTowers }
}

// function tileValueForMaximizeTowerDamage(tower: AttackingTargetedTower, target: RC, mobs: Mob[]) {
//   const damageScale = 1,
//     angleScale = 1
//   const healthSum = mobs.reduce((accum, mob) => accum + mob.health.current, 0)
//   const damageSum = tower.attacks.reduce((accum, attack) => accum + attack.damage, 0)
//   const newAngle = rcAngle(tower, target)
//   const deltaAngle = newAngle - tower.angle

//   return damageScale * Math.min(damageSum, healthSum) - Math.abs(angleScale * deltaAngle)
// }

// function tileValueForClosestToHQ(tower: TargetedTower, target: RC, mobs: Mob[]) {
//   if (!mobs.length) return -Infinity
//   return -mobs[0].distanceFromEnd
// }

function doTowerAttacks(tick: number, tower: AttackingTower & TargetedTower): AttackFrom[] | undefined {
  //console.log(tower)
  if (!tower.attacks) return
  let anyAttacked = false
  const newAttacks = tower.attacks.map((attack) => {
    //console.log("tim eto attk", tick, attack.lastFiredAt, attack.interAttackTicks)
    if (tick > attack.lastFiredAt + attack.interAttackTicks) {
      anyAttacked = true
      return { ...attack, lastFiredAt: tick }
    }
    return attack
  })
  if (anyAttacked) return newAttacks
}

import PriorityQueue from "ts-priority-queue"
import { distance } from "./grid"
import { Direction, RC, WithDirection } from "./types"

// From https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode

export function PointToS(p: RC): string {
  return `${p.row}X${p.col}`
}

const distanceFunc = distance
// const distanceFunc = (start: Point, goal: Point): number =>
//   Math.sqrt(Math.pow(goal.x - start.x, 2) + Math.pow(goal.y - start.y, 2));

function scalePoint<P extends RC>(p: P, scalingFactor: number): P {
  return {
    ...p,
    row: Math.floor(p.row / scalingFactor),
    col: Math.floor(p.col / scalingFactor),
  }
}

// const scalePoint = (p: Point, scalingFactor: number): Point => ({
//   x: p.x,
//   y: p.y,
// });
// GoalFunc should return true if this is a goal state
// type GoalFunc = (p: Point) => boolean;
export function BestPath(
  scalingFactor: number,
  start: RC,
  goal: RC,
  goalRadius: number,
  score: ScoreFunc
  //goalFunc: GoalFunc
): WithDirection<RC>[] {
  //console.warn("Pathing", start, goal)
  const cameFrom = new Map<string, RC>(),
    gScore = new Map<string, number>(),
    fScore = new Map<string, number>(),
    openSetSet = new Set<string>(),
    openSet = new PriorityQueue<RC>({
      comparator: function (a: RC, b: RC) {
        return (fScore.get(PointToS(a)) || 0) - (fScore.get(PointToS(b)) || 0)
      },
    })

  start = scalePoint(start, scalingFactor)
  goal = scalePoint(goal, scalingFactor)
  goalRadius = (goalRadius - 1) / scalingFactor

  openSet.queue(start)
  openSetSet.add(PointToS(start))

  gScore.set(PointToS(start), 0)

  let maxIter = 100000

  while (openSet.length > 0) {
    if (maxIter-- < 0) {
      console.log("max iter reached")
      console.log(openSet.length, openSetSet.size)
      console.log(openSetSet)
      console.log(gScore)
      console.log(fScore)
      return []
    }
    const current = openSet.dequeue(),
      currentS = PointToS(current)

    openSetSet.delete(currentS)

    if ((current.row === goal.row && current.col === goal.col) || distanceFunc(current, goal) < goalRadius) {
      return reconstructPath(cameFrom, current)
    }
    scoreNeighbors(scalingFactor, current, score).forEach(([neighbor, score]) => {
      if (PointToS(neighbor) !== PointToS(goal) && score === Infinity) return
      const neighborS = PointToS(neighbor)

      const localGScore = (gScore.get(currentS) ?? Infinity) + score
      if (localGScore < (gScore.get(neighborS) ?? Infinity)) {
        cameFrom.set(neighborS, current)
        gScore.set(neighborS, localGScore)
        fScore.set(neighborS, localGScore + distanceFunc(neighbor, goal))
        if (!openSetSet.has(neighborS)) {
          openSetSet.add(neighborS)
          openSet.queue(neighbor)
        }
      }
    })
  }

  return []
}

export type ScoreFunc = (at: RC) => number

function scoreNeighbors(scalingFactor: number, n: RC, score: ScoreFunc): [RC, number][] {
  return [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
  ].map(([row, col]) => {
    row = n.row + row
    col = n.col + col
    return [{ row, col }, score(scalePoint({ row, col }, 1 / scalingFactor))]
  })
}

function relativeDirection(from: RC, to: RC): Direction {
  if (from.col > to.col) {
    return "L"
  }
  if (from.col < to.col) {
    return "R"
  }
  if (from.row > to.row) {
    return "U"
  }
  if (from.row < to.row) {
    return "D"
  }
  return "S"
}

function reconstructPath(cameFrom: Map<string, RC>, current: RC): WithDirection<RC>[] {
  const totalPath: WithDirection<RC>[] = [{ ...current, direction: "S" }]
  while (cameFrom.has(PointToS(current))) {
    const prev = cameFrom.get(PointToS(current)) as RC
    totalPath.unshift({ ...prev, direction: relativeDirection(prev, current) })
    current = prev
  }
  return totalPath
}

export function PrintPath(path: RC[], scalingFactor = 1): string {
  let maxC = -Infinity,
    maxR = -Infinity,
    minC = Infinity,
    minR = Infinity
  const pathMap = new Map<string, number>()

  path.forEach((p, idx) => {
    p = scalePoint(p, scalingFactor)
    maxC = Math.max(maxC, p.col)
    maxR = Math.max(maxR, p.row)
    minC = Math.min(minC, p.col)
    minR = Math.min(minR, p.row)
    pathMap.set(PointToS(p), idx)
  })

  let output = ""
  for (let row = minR; row <= maxR; row++) {
    let line = ""
    for (let col = minC; col <= maxC; col++) {
      line += pathMap.get(PointToS({ row, col })) !== undefined ? "X" : "0"
    }
    output += line + "\n"
  }
  return output
}

import FNV from "fnv-lite"
import type { RC, WithDirection } from "./types"

export function poisson(mean: number) {
  const L = Math.exp(-mean)
  let p = 1.0
  let k = 0

  do {
    k++
    p *= Math.random()
  } while (p > L)

  return k - 1
}

export function hash(path: WithDirection<RC>[]): any {
  return FNV.base36(JSON.stringify(path))
}

export function shuffle<T>(array: T[]) {
  // From https://stackoverflow.com/a/2450976
  let currentIndex = array.length,
    randomIndex

  // While there remain elements to shuffle.
  while (currentIndex != 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex--

    // And swap it with the current element.
    ;[array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]]
  }

  return array
}

export function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined
}

export function getCurrentTick() {
  return new Date().getTime() / 33
}

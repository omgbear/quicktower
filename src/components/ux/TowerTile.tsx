import { elementToColor } from "../../lib/elemental"
import { addDistanceAtAngle, GRID_SIZE, RcToString, RCToXY } from "../../lib/grid"
import { isTargetedTower, TargetedTower, Tower } from "../../lib/types"
import { tileEl } from "./tileEl"

function Barrel({ tower }: { tower: TargetedTower }) {
  const { x, y } = RCToXY(tower, GRID_SIZE / 2)
  /* const target = tower.target ? RCToXY(tower.target) : { x: 0, y: 0 }

   * const cx = topLeft.x + GRID_SIZE / 2
   * const cy = topLeft.y + GRID_SIZE / 2

   * const distance = xyDistance({ x: cx, y: cy }, target)
   * const unitDistX = ((target?.x || 0) - cx) / distance,
   *   unitDistY = ((target?.y || 0) - cy) / distance
   * const gunLength = 20
   * const gunX = cx + gunLength * unitDistX,
   *   gunY = cy + gunLength * unitDistY */

  const { x: gunX, y: gunY } = addDistanceAtAngle({ x, y }, tower.angle, 20)

  return <line x1={x} y1={y} x2={gunX} y2={gunY} stroke="black" strokeWidth="5" />
}

export function TowerTile({ tower }: { tower: Tower }) {
  const fill = tower.subkind == "HQ" ? "purple" : elementToColor[tower.element]
  const healthRatio = tower.health.current / tower.health.max
  const opacity = Math.max(0.2, 0.9 * healthRatio)
  const topLeft = RCToXY(tower)

  const cx = topLeft.x + GRID_SIZE / 2
  const cy = topLeft.y + GRID_SIZE / 2

  const barrel = isTargetedTower(tower) ? <Barrel tower={tower} /> : undefined

  return (
    <g key={`g-${RcToString(tower)}`}>
      {tileEl(
        tower,
        {
          fill: healthRatio > 0.4 ? fill : "red",
          mask: "url(#mask-checkers)",
          opacity: 1 - healthRatio,
        },
        "mask"
      )}
      {tileEl(tower, { fill, opacity: opacity }, "solid")}
      <circle cx={cx} cy={cy} r={GRID_SIZE / 4} fill="none" stroke="black" strokeWidth="5" />
      {barrel}
      <text x={cx} y={cy + 30} fontSize="24" stroke="red">
        {isTargetedTower(tower) ? tower.angle.toFixed(2) : "-"}
      </text>
    </g>
  )
}

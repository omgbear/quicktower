import React from "react"
import { getXYExtantsOfFan, Hitboxes } from "../../lib/attack"
import { elementToColor } from "../../lib/elemental"
import { GRID_COUNT, GRID_SIZE, RCToXY, StringToRc } from "../../lib/grid"
import { ImmutableMap, ImmutableSet } from "../../lib/immutable"
import { AttackInstance, Mob, RC, Spawner, Tower } from "../../lib/types"

import { tileEl } from "./tileEl"
import { TowerTile } from "./TowerTile"

const gridLines = (
  <g>
    {Array(GRID_COUNT)
      .fill(0)
      .map((v, idx) => {
        return [
          <rect
            key={`y-${idx}`}
            x={GRID_SIZE * (idx + 1)}
            y={0}
            height="100%"
            width="1px"
            stroke="black"
            opacity="0.1"
          />,
          <rect
            key={`x-${idx}`}
            y={GRID_SIZE * (idx + 1)}
            x={0}
            height="1px"
            width="100%"
            stroke="black"
            opacity="0.1"
          />,
        ]
      })
      .flat()}
  </g>
)

const defs = (
  <defs>
    <pattern
      id="pattern-checkers"
      x="0"
      y="0"
      width="20"
      height="20"
      viewBox="0 0 2 2"
      patternUnits="userSpaceOnUse"
    >
      <rect fill="white" x="0" width="1" height="1" y="0"></rect>
      <rect fill="white" x="1" width="1" height="1" y="1"></rect>
    </pattern>
    <mask id="mask-checkers" x="0" y="0" width="2" height="2">
      <rect x="0" y="0" width="1000" height="1000" fill="url(#pattern-checkers)" />
    </mask>
  </defs>
)

export interface GameGridProps {
  hoverRowCol: RC | undefined
  placedTowers: ImmutableMap<string, Tower>
  pendingTowers: ImmutableMap<string, Tower>
  paths: ImmutableMap<string, RC[]>
  mobs: ImmutableSet<Mob>
  spawners: Spawner[]
  hitboxes: ImmutableSet<Hitboxes>
  attacks: ImmutableSet<AttackInstance>
  onMouseMove: React.MouseEventHandler
  onMouseUp: React.MouseEventHandler
  onMouseOut: React.MouseEventHandler
}
export function GameGrid({
  hoverRowCol,
  placedTowers,
  pendingTowers,
  paths,
  onMouseMove,
  onMouseOut,
  onMouseUp,
  mobs,
  attacks,
  hitboxes,
  spawners,
}: GameGridProps) {
  const hoverTopTile = hoverRowCol ? tileEl(hoverRowCol, { fill: "blue", opacity: "0.5" }) : undefined

  const placedTileEls = placedTowers.valueSeq().map((t: Tower) => {
    return <TowerTile tower={t} />
  })

  const pendingTileEls = pendingTowers.valueSeq().map((t: Tower) => {
    const color = elementToColor[t.element]
    const fill = hoverRowCol ? color : "darkred"
    return tileEl(t, { fill, opacity: 0.5 })
  })

  const mobEls = mobs.valueSeq().map((m: Mob) => {
    const pos = RCToXY(m)
    pos.x += m.offsetX
    pos.y += m.offsetY
    return <circle key={m.id} cx={pos.x} cy={pos.y} r={2} strokeWidth="5" stroke="blue" />
  })

  const pathEls = paths
    .entrySeq()
    .map(([pathName, path]) => (
      <path
        d={pathFromRC(path)}
        key={pathName}
        strokeWidth="5"
        strokeLinejoin="round"
        stroke="darkgreen"
        fill="none"
      />
    ))

  const attackEls = attacks.map((attack) => (
    <path
      key={attack.id}
      d={pathFromAttack(attack)}
      strokeWidth="5"
      stroke={elementToColor[attack.element]}
      fill={elementToColor[attack.element]}
      fillOpacity="0.6"
      strokeOpacity="0.8"
    />
  ))

  const hitboxEls = hitboxes.map((hitbox) => {
    return hitbox.rcs
      .map((rcS) => {
        const topLeft = StringToRc(rcS)
        const { x, y } = RCToXY(topLeft)
        return (
          <rect
            x={x}
            y={y}
            height={GRID_SIZE}
            width={GRID_SIZE}
            stroke="grey"
            fill="grey"
            fillOpacity="0.1"
            strokeOpacity="0.3"
          />
        )
      })
      .concat(
        hitbox.quartiles.map((rcS) => {
          const topLeft = StringToRc(rcS)
          const { x, y } = RCToXY(topLeft)
          return (
            <rect
              x={x}
              y={y}
              height={GRID_SIZE / 2}
              width={GRID_SIZE / 2}
              stroke="purple"
              fill="purple"
              fillOpacity="0.3"
              strokeOpacity="0.6"
            />
          )
        })
      )
      .flat()
  })

  const spawnerEls = spawners.map((spawner) => {
    const { x, y } = RCToXY(spawner)
    return <image y={y} x={x} width={GRID_SIZE} height={GRID_SIZE} href={`spawner.png`} />
  })

  return (
    <svg
      onMouseMove={onMouseMove}
      onMouseUp={onMouseUp}
      onMouseOut={onMouseOut}
      className="bg-gray-100"
      height={GRID_COUNT * GRID_SIZE}
      width={GRID_COUNT * GRID_SIZE}
      style={{
        margin: "0px auto",
        borderLeft: "12px black solid",
        borderRight: "12px black solid",
        borderBottom: "12px black solid",
        boxSizing: "content-box",
        borderTop: "12px white solid",
        userSelect: "none",
      }}
    >
      <g style={{ pointerEvents: "none" }}>
        {attackEls}
        <g>{hitboxEls}</g>
        {defs}
        {pathEls}
        {spawnerEls}
        {hoverTopTile}
        {pendingTileEls}
        {placedTileEls}
        <g>{mobEls}</g>
        {gridLines}
      </g>
    </svg>
  )
}

function pathFromRC(rc: RC[]): string {
  return (
    "M" +
    rc
      .map(({ row, col }) => {
        return `${(col + 0.5) * GRID_SIZE} ${(row + 0.5) * GRID_SIZE}`
      })
      .join(" ")
  )
}

function pathFromAttack(attack: AttackInstance): string {
  const { farStart, farEnd, closeEnd, closeStart } = getXYExtantsOfFan(attack) // TODO: Cache on attack itself

  //  console.log(attack.origin, farEnd, farStart)
  return (
    "M" +
    [closeEnd, closeStart, farStart, farEnd, closeEnd]
      .map(({ x, y }) => {
        return `${x} ${y}`
      })
      .join(" ")
  )
}

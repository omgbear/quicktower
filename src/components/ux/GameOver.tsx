import { Dialog } from "@headlessui/react"

function survivorText(secondsSurvived: number) {
  return (
    <span>
      You survived <span className="font-bold">{secondsSurvived.toFixed(2)} seconds</span>!
    </span>
  )
}

export function GameOver({
  secondsSurvived,
  isOpen,
  cancel,
  next,
  won,
}: {
  isOpen: boolean
  cancel: () => void
  next: () => void
  secondsSurvived: number
  won: boolean
}) {
  const title = won ? "Congrats!" : "Bummer, Game Over!"
  const nextButtonText = won ? "Next Level" : "Another Go"

  return (
    <Dialog open={isOpen} onClose={() => cancel()} className="relative z-50">
      <div className="fixed inset-0 bg-black/30" aria-hidden="true" />

      <div className="fixed inset-0 flex-row justify-center">
        <div className="ring-8 ring-slate-800 max-w-sm mx-auto">
          <Dialog.Panel className="w-full rounded bg-slate-300 ring-4 ring-inset mt-64 p-3">
            <Dialog.Title className="text-3xl">{title}</Dialog.Title>
            <Dialog.Description className="text-lg ml-8 mt-4">
              {survivorText(secondsSurvived)}
            </Dialog.Description>

            <button className="ml-14 mt-10 btn btn-blue ring-2 ring-indigo-500" onClick={cancel}>
              Main Menu
            </button>
            <button className=" btn btn-green ml-6 mb-2 ring-2 ring-emerald-500" onClick={next}>
              {nextButtonText}&nbsp;&nbsp;⮚⮞
            </button>
          </Dialog.Panel>
        </div>
      </div>
    </Dialog>
  )
}

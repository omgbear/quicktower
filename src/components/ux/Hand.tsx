import { Card, GetCard } from "../../lib/card"
import { InHand } from "../../lib/hand"
import { ImmutableList } from "../../lib/immutable"
import { elementToColor } from "../../lib/elemental"
import { GRID_SIZE } from "../../lib/grid"

export type HandProps = {
  cards: ImmutableList<InHand<Card>>
}

export function Hand(props: HandProps) {
  const { cards } = props
  const width = 90,
    height = 125,
    xSpacing = 8
  const cardEls = cards.map((card, idx) => {
    const y = card.raised == "all" ? 0 : card.raised == "some" ? 5 : 20
    const x = idx * (width + xSpacing) + 70
    return (
      <>
        <rect
          key={JSON.stringify(card)}
          y={y}
          x={x}
          width={width}
          height={height}
          fill="white"
          rx="10"
          filter={`drop-shadow(5px 5px 2px ${elementToColor[card.element]})`}
        ></rect>
        <text dominantBaseline="middle" textAnchor="middle" stroke="black" y={y + 20} x={x + GRID_SIZE - 5}>
          {card.name}
        </text>
        <image y={y + 30} x={x + 24} width="50" height="50" href={`element-${card.element}.png`} />
        <image y={y + 82} x={x + 29} width="40" height="40" href={`icon-${card.subkind}.png`} />
      </>
    )
  })
  return (
    <svg className="mx-auto" width={(width + xSpacing) * 12}>
      {cardEls}
    </svg>
  )
}

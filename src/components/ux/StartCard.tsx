export function StartCard({ setRunning }: { setRunning: (b: boolean) => void }) {
  return (
    <div className="w-full h-full">
      <div className="left-64 relative w-96 top-32 aspect-video bg-slate-200 flex-row  opacity-90 p-8  ">
        <div className="text-6xl">QuickTower</div>
        <button onClick={() => setRunning(true)} className="btn btn-blue w-40 mt-64 ml-40">
          Start
        </button>
      </div>
    </div>
  )
}

import React from "react"
import { GRID_SIZE, RcToString, RCToXY } from "../../lib/grid"
import { RC } from "../../lib/types"

export function tileEl(t: RC, props: React.SVGProps<SVGRectElement>, keySuffix = "") {
  const topLeft = RCToXY(t)
  const rcStr = RcToString(t)
  return (
    <rect
      key={rcStr + keySuffix}
      style={{ pointerEvents: "none" }}
      {...topLeft}
      {...props}
      width={GRID_SIZE}
      height={GRID_SIZE}
    />
  )
}

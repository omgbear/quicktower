import React, { useCallback, useMemo, useState } from "react"
import { AttackHit, getHitboxes, useAttacks } from "../../lib/attack"
import { Card, GetCard, TowerCard } from "../../lib/card"
import { playCardsFn, useDeck } from "../../lib/deck"
import { coordsEqual, midCoords, RcToString, XYToRC } from "../../lib/grid"
import { InHand } from "../../lib/hand"
import { ImmutableMap, ImmutableSet } from "../../lib/immutable"
import { useLevels } from "../../lib/levels"
import { useMobs } from "../../lib/mobs"
import { useInterval } from "../../lib/reactUtils"
import { newTower, newTowerFromCard, useTowers } from "../../lib/towers"
import { Elemental, Level, RC, Spawner, Tower } from "../../lib/types"
import { getCurrentTick } from "../../lib/util"
import { GameGrid } from "../ux/GameGrid"
import { GameOver } from "../ux/GameOver"
import { Hand } from "../ux/Hand"
import { EditableField } from "./editable_field"
const TicksPerSecond = 30

const initialDeck = [
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("firetower"),
  GetCard("watertower"),
  GetCard("watertower"),
  GetCard("watertower"),
]

const turbo = 1

export function GameController({
  gameComplete,
  exitToMainMenu,
  currentLevel,
  currentLevelIdx,
}: {
  gameComplete: (won: boolean | number) => void
  exitToMainMenu: () => void
  currentLevel: Level
  currentLevelIdx: number
}) {
  const [hoverRowCol, setHoverRowCol] = useState<RC | undefined>()
  const [pendingTowers, setPendingTowers] = useState<ImmutableMap<string, Tower>>(ImmutableMap())
  const [isDragging, setIsDragging] = useState<boolean>(false)
  const [pendingCardIdxs, setPendingCardIdxs] = useState(ImmutableSet<number>())

  const [startTime] = useState(Date.now())
  const { deck, draw, play, discard } = useDeck(initialDeck)
  const { resolveAttacks, startAttacks, attacks } = useAttacks({})

  const cardFromHand = useCallback(
    (offset: number) => {
      return deck.hand.get(offset)
    },
    [deck.hand]
  )
  const timeLeftMs = Math.max(0, turbo * currentLevel.gameLengthMs - (Date.now() - startTime))

  const { getPlacedTower, placeTowers, attackTowers, placedTowers, updateTowers } = useTowers()
  const gameOverAt = useMemo(
    () => (timeLeftMs <= 0 || !(getPlacedTower(midCoords)?.subkind == "HQ") ? Date.now() : false),
    [getPlacedTower, timeLeftMs]
  )

  // MOBS
  // TODO: Run attacks separately.
  // allow towers to attack mobs
  // maybe redo mob attacks to also use useAttacks
  const { mobs, paths, updateMobs } = useMobs(currentLevel.spawners, getPlacedTower, attackTowers)
  useInterval(
    gameOverAt
      ? () => false
      : () => {
          const tick = getCurrentTick()
          updateTowers(
            mobs,
            startAttacks,
            hoverRowCol ? { ...hoverRowCol, offsetX: 25, offsetY: 25 } : undefined
          )
          const { hits }: { hits: AttackHit[] } = resolveAttacks(tick, [...mobs])
          updateMobs(tick, hits)
        },
    1000 / TicksPerSecond
  )
  //useInterval(gameOverAt ? () => false : () => , 1000 / TicksPerSecond)
  // useInterval(gameOverAt ? () => false : () => resolveAttacks(getCurrentTick(),towe), 1000 / TicksPerSecond)

  // TOWER

  // On click, place tower
  // On move, if on a connecting cardinal direction, then place the next tower
  // Once placed 5, can place bonus tower 6, if you go to 6, then start consuming additional cards

  const addPendingTower = useCallback(
    function addPendingTower(coords: RC) {
      const rcStr = RcToString(coords)
      const nextCard = cardFromHand(pendingTowers.size)
      if (!nextCard) return
      if (!pendingTowers.get(rcStr) && !getPlacedTower(coords)) {
        setPendingTowers(pendingTowers.set(rcStr, newTowerFromCard(coords, nextCard as TowerCard)))
        setPendingCardIdxs(pendingCardIdxs.add(pendingTowers.size))
      }
    },
    [pendingTowers, setPendingTowers, getPlacedTower, cardFromHand]
  )

  const canPlaceTowerAt = useCallback(
    function canPlaceTowerAt(coords: RC) {
      return !(
        getPlacedTower(coords) ||
        currentLevel.spawners.find(({ row, col }) => coords.row == row && coords.col == col)
      )
    },
    [getPlacedTower, currentLevel]
  )

  const onMouseMove = useCallback(
    function onMouseMove(evt: React.MouseEvent<SVGSVGElement>) {
      function onTileEnter(buttons: number, prevCoords: RC | undefined) {
        if (buttons && prevCoords) {
          addPendingTower(prevCoords)
        }
      }

      const startDrag = evt.buttons && !isDragging
      if (startDrag) {
        setIsDragging(true)
      } else if (isDragging && !evt.buttons) {
        setIsDragging(false)
      } else if (!evt.buttons && pendingTowers.size) {
        setPendingTowers(pendingTowers.clear())
        setPendingCardIdxs(pendingCardIdxs.clear())
      }
      const newCoords = XYToRC({ x: evt.nativeEvent.offsetX, y: evt.nativeEvent.offsetY })
      if (startDrag || !coordsEqual(hoverRowCol, newCoords)) {
        onTileEnter(evt.buttons, newCoords)
      }

      if (canPlaceTowerAt(newCoords)) {
        setHoverRowCol(newCoords)
      } else {
        setHoverRowCol(undefined)
      }
    },
    [pendingTowers, getPlacedTower]
  )

  const GIGA_SIZE = 8
  const placingGigaTower = pendingTowers.size >= GIGA_SIZE
  const gigaTowerKind = "fire"
  async function onMouseUp(evt: React.MouseEvent<SVGSVGElement>) {
    if (hoverRowCol) {
      addPendingTower(hoverRowCol)
      if (placingGigaTower) {
        await play(playCardsFn(0, pendingTowers.size, "burnt"), pendingTowers.size == deck.hand.size ? 7 : 0)
        placeTowers(toGigaTower(pendingTowers, gigaTowerKind))
      } else {
        await play(
          playCardsFn(0, pendingTowers.size, "discard"),
          pendingTowers.size == deck.hand.size ? 7 : 0
        )
        placeTowers(pendingTowers)
      }
    }
    setPendingTowers(pendingTowers.clear())
    setPendingCardIdxs(pendingCardIdxs.clear())
  }

  function onMouseOut(evt: React.MouseEvent<SVGSVGElement>) {
    setHoverRowCol(undefined)
  }

  function addCardUi(card: Card, idx: number): InHand<Card> {
    return {
      ...card,
      raised: pendingCardIdxs.has(idx) ? "some" : "none",
    }
  }

  const hitboxes = attacks.map(getHitboxes)

  const won = timeLeftMs <= 0

  const debugPanel = (
    <div className="border-2-red bg-white fixed">
      <div>Debug Panel</div>
      <div>
        <EditableField label="Level" value={currentLevelIdx} setValue={(v) => gameComplete(v)} />
      </div>
    </div>
  )

  return (
    <div>
      {debugPanel}
      <GameOver
        won={won}
        isOpen={Boolean(gameOverAt)}
        cancel={exitToMainMenu}
        next={() => gameComplete(won)}
        secondsSurvived={((gameOverAt || 0) - startTime) / 1000}
      />
      <div className="text-3xl text-white text-center w-full">Time Left {(timeLeftMs / 1000).toFixed(1)}</div>
      <GameGrid
        hoverRowCol={hoverRowCol}
        paths={paths}
        pendingTowers={pendingTowers}
        placedTowers={placedTowers}
        mobs={mobs}
        attacks={attacks}
        onMouseMove={onMouseMove}
        onMouseOut={onMouseOut}
        onMouseUp={onMouseUp}
        hitboxes={hitboxes}
        spawners={currentLevel.spawners}
      />
      <Hand cards={deck.hand.map(addCardUi)} />
    </div>
  )
}

function toGigaTower(
  pendingTowers: ImmutableMap<string, Tower>,
  gigaTowerKind: Elemental
): ImmutableMap<string, Tower> {
  return pendingTowers.map((tower) => {
    return newTower(tower, gigaTowerKind, "gigatower", 2_000)
  })
}

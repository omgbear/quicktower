export type EditableFieldProps<T extends number | string> = {
  value: T
  setValue: (val: T) => void
  label: string
}
export function EditableField<T extends number | string>({ value, setValue, label }: EditableFieldProps<T>) {
  const isNumber = typeof value == "number"
  return (
    <div>
      <span>{label}: </span>
      <input
        type={isNumber ? "number" : "text"}
        value={value}
        onChange={(e) => setValue((isNumber ? parseInt(e.target.value) : e.target.value) as T)}
      />
    </div>
  )
}

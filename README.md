# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

# Todo Notes

- Cards!
- Have a hand, max of 7?
- When you play at least X, turn into a mega-tower
- Tower kind based mostly on card type?
- Get cards between encounters
- Start with mostly hedge?
- Monster hedge when mostly hedge, should be pretty good
- Better quality towers can imbue mega tower with specials
- Mega tower takes up many spaces (Really many little mega towers, maybe coordinated somehow)
- Maybe some cards place multiple spaces
- Hand manipulation?
- Deck manipulation?
- build cards as enemies enter the board?
- OR get reshuffles over time / from kills
- Maybe towers always shoot at mouse cursor!
- Shoot on some interval, countdown till fire, so you can position properly.
- Need to build a good maze for this?
- Need good mob grouping for this -- hard levels will be more spaced out
- Destroy towers somehow maybe hold mouse down

# Level traits

- Spawner locations [totally random?? outside central ring? Maybe protection radius decreases over time]
- Monster traits
- Time limit

## Monster traits

- size
- speed
- health
- damage
- value
- range??
